<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use Illuminate\Http\Request;
use App\Models\Produk;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $expDate = DB::table('carts')->whereRaw('DATEDIFF(CURDATE(),DATE_FORMAT(created_at,"%Y-%m-%d")) AND DATEDIFF(CURDATE(),DATE_FORMAT(created_at,"%Y-%m-%d")) < 5 ')->get();

        return view('cart.index', [
            'title' => "Detail Pesanan",
            'cart'     => Cart::where('user_id', '=', auth()->user()->id)->get(),
            'subtotal' => Cart::where('user_id', '=', auth()->user()->id)->sum('harga')
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Produk $produk)
    {
        return view('cart.create', [
            'title' => 'Booking',
            'produk' => $produk
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Produk $produk)
    {
        $tblproduk =  Produk::find($produk->id);

        $data = [
            'produk_id'          => $produk->id,
            'harga'  => $produk->harga,
            'user_id'       => auth()->user()->id,
        ];
        $stok = $tblproduk->stok - 1;
        Cart::create($data);
        Produk::where('id', $produk->id)->update(['stok' => $stok]);
        return redirect('/cart');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */

    public function destroy(Cart $cart)
    {

        $tblproduk =  Produk::find($cart->produk_id);
        $stok = $tblproduk->stok + 1;
        Cart::destroy($cart->id);
        Produk::where('id', $cart->produk_id)->update(['stok' => $stok]);
        return redirect('/cart')->with('delete', 'Data berhasil di hapus');
    }
}
